package com.empirefree.mybatisplus;

import com.google.common.primitives.Ints;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @program: mvn
 * @description:
 * @author: huyuqiao
 * @create: 2022/08/18 10:18
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class GuavaTest {
    @Test
    public void basicTypeTest(){
        int[] array = new int[]{1, 3, 5, 7, 9};
        System.out.println(Ints.max(array) + Ints.min(array));
    }
}
