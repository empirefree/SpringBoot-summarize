package com.empirefree.mybatisplus;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONObject;
import com.empirefree.mybatisplus.entity.Person;
import com.empirefree.mybatisplus.entity.SonPerson;
import com.empirefree.mybatisplus.entity.User;
import com.sun.org.apache.bcel.internal.generic.NEW;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.poi.ss.formula.functions.T;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import sun.reflect.generics.tree.Tree;

import javax.swing.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @program: mvn
 * @description:
 * @author: huyuqiao
 * @create: 2022/03/16 10:41
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class JavaTest {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RestTemplate restTemplate;

    private final static User user = new User();    //对象地址不可改变，值却可以改变
    @Test
    public void dateTest(){
        user.setName("asdf");
        user.setId(1);
        user.setName("a");
        System.out.println(user);
    }
    @Test
    public void restTemplateTimeoutTest(){
        long time = System.currentTimeMillis();
        try {
            String insertUrl = "https://ewmmsstaff.dyson.cn/outside/single/dialogue";

            JSONObject params = new JSONObject();
            params.put("date", "2022-07-06");
            params.put("page", 1);
            params.put("page_size", 1000);
            HttpHeaders headers = new HttpHeaders();
            MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
            headers.setContentType(type);
            headers.add("Accept", MediaType.APPLICATION_JSON.toString());
            headers.add("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
            headers.add("token", "C447008ED2D9A398B626511D3000DF02FCE920C15BA7DE3E5EE9A3AEF7286645");
            HttpEntity<String> formEntity = new HttpEntity<>(params.toString(), headers);
            JSONObject result = restTemplate.postForEntity(insertUrl, formEntity, JSONObject.class).getBody();

        } catch (RestClientException e) {
            System.out.println("执行时间" + (System.currentTimeMillis() - time));
            e.printStackTrace();
        }
    }

    private void test(){
        String userId = "huyuqiao";
        String externalUserId = "陈牛逼";
        String welcomeCode = "臭傻逼";
        // fix:防止企微并发发送多次添加客户消息，导致后续处理报错
        String key = "WeChatCallback:" + userId + externalUserId + null;
        Boolean successMsg = stringRedisTemplate.opsForValue().setIfAbsent(key, "", 20, TimeUnit.SECONDS);
        if (!successMsg){
            System.out.println("不参与竞争");
            return;
        }
    }
    @Test
    public void redisLock(){
        for (int i = 0; i < 100; i++) {
            new Thread(() ->{
                test();
            }).start();
        }
    }



    @Test
    public void transformStreamTest() throws Exception {
        FileInputStream fileInputStream = new FileInputStream("hello.txt");
        FileOutputStream fileOutputStream = new FileOutputStream("hello-3.txt");

        InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);


        char[] buffer = new char[20];
        int len;
        while ((len = inputStreamReader.read(buffer)) != -1){
            String str = new String(buffer, 0, len);
            System.out.println(str);

            outputStreamWriter.write(buffer, 0, len);
        }
        inputStreamReader.close();
        outputStreamWriter.close();
    }


    @Test
    public void fileInputOutputStreamTest(){
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;

        try {
            // 1、获取非文本文件
            File srcFile = new File("1631533963.jpg");
            File destFile = new File("1631533963-2.jpg");

            // 2、搭建管道
            fileInputStream = new FileInputStream(srcFile);
            fileOutputStream = new FileOutputStream(destFile);

            // 2.2搭建缓冲流
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);


/*            // 3、读取数据
            byte[] buffer = new byte[1024];
            int len;
            while ((len = fileInputStream.read(buffer)) != -1){
                fileOutputStream.write(buffer, 0, len);;
            }*/

            // 3、采用缓冲流（内部提供了缓冲区，速度更快，自动刷新写出写入）读取
            byte[] bytes = new byte[1024];
            int len;
            while ((len = bufferedInputStream.read(bytes)) != -1){
                bufferedOutputStream.write(bytes, 0, len);
            }

            //由于先获取的filestream，再包装了bufferstream,所以关闭是先从外面开始关闭，buffer关闭了内部filestream也自动关闭
            bufferedInputStream.close();
            bufferedOutputStream.close();


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           /* // 4、关闭非文本文件
            if (fileInputStream != null){
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fileOutputStream != null){
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }*/


        }

    }

    @Test
    public void fileReaderFileWriter(){
        // 1、获取文本文件
        File srcFile = new File("hello.txt");
        File destFile = new File("hello-2.txt");

        // 2、搭建管道
        FileReader fileReader = null;
        FileWriter fileWriter = null;
        try {
            fileReader = new FileReader(srcFile);
            fileWriter = new FileWriter(destFile);

            BufferedReader bufferedReader = new BufferedReader(fileReader);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            // 3、操作数据
/*            char[] buffer = new char[1024];
            int len;
            while ((len = fileReader.read(buffer)) != -1){
                fileWriter.write(buffer, 0, len);
            }*/
            String data;
            // 3、方式二：使用String
            while ((data = bufferedReader.readLine()) != null){
                //bufferedWriter.write(data + "\n");
                bufferedWriter.write(data);
                bufferedWriter.newLine();
            }
            bufferedReader.close();
            bufferedWriter.close();

        }  catch (IOException e) {
            e.printStackTrace();
        } finally {
            //4、关闭文件
            try {
                fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                fileWriter.close();
            } catch (IOException e) {
            }
        }



    }




    @Test
    public void fileTest(){
        //标识某个文件下面的文件夹或者文件。文件夹其实本身也是文件
        File asdf = new File("D:\\", "asdf");
        System.out.println(asdf);

        //文件的读写权限
        File file = new File("C:\\Users\\EDY\\Desktop\\CRM-接口.txt");
        System.out.println(file.canRead());
        System.out.println(file.canWrite());
    }


    @Test
    public void collectionsTest(){

        //栈stack确实存在，vector子类，实现list接口
        Stack<Object> objects2 = new Stack<>();

        ArrayList<Object> objects = new ArrayList<>();
        objects.add("123");
        objects.add("123");
        objects.add("123");
        List<Object> list = Arrays.asList(new Object[objects.size()]);
        Collections.copy(list, objects);
        for (Object obj : list){
            System.out.println(obj);
        }

        //单纯synchronized修饰
        Collection<Object> objects1 = Collections.synchronizedCollection(list);

    }


    @Test
    public void PropertiesTest(){
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream("propertiesTest.properties");
            Properties properties = new Properties();
            properties.load(fileInputStream);

            // alt + enter快捷键
            String name = properties.getProperty("name");
            String age = properties.getProperty("age");
            System.out.println("name: " + name + ", age=" + age);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null){
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Test
    public void mapTest(){
        TreeMap<Object, Object> objectObjectTreeMap = new TreeMap<>();

        System.out.println(objectObjectTreeMap.put("1", 2));
        System.out.println(objectObjectTreeMap.put("1", 3));
        objectObjectTreeMap.put("3", 4);
        for (Map.Entry entry : objectObjectTreeMap.entrySet()){
            System.out.println(entry.getKey() + " " + entry.getValue());
        }


        HashMap<Object, Object> objectObjectHashMap = new HashMap<>();
        objectObjectHashMap.put(null, null);
        new HashSet<>();
        System.out.println(objectObjectHashMap.put("1", 3));
        System.out.println(objectObjectHashMap.containsKey("1"));   //是否包含某个key
        System.out.println(objectObjectHashMap.containsValue("3")); //是否包含某个value
        System.out.println(objectObjectHashMap.put("1", 2));


        new LinkedHashMap<>();
    }

    @Test
    public void hashSetTest(){
        /**
         * 由于集合只涉及添加，contains,remove时只设计到equals方法比较
         * 而hashset添加涉及到hashcode，equals的比较，所以集合可以只重写equals方法，而hashset需要重写hashcode和equals。
         */
        //HashSet<Person> objects = new HashSet<>();
        Collection objects= new ArrayList();
        System.out.println(objects.add(new Person(1, 2, "2")));
        System.out.println(objects.contains(new SonPerson(1, 2, "2")) + "AAAa");
        System.out.println(objects.add(new Person(1, 2, "2")));
        System.out.println(objects.size());
/*        TreeSet treeSet = new TreeSet<>();
        treeSet.add("123");
        treeSet.add(123);*/
    }


    @Test
    public void collectionContainsTest() {
        // 若是重写了equals方法，比较的就是内容，否则就是调用object的==
        Collection collection = new ArrayList();
        collection.add(new Person(1, 12, "name"));
        collection.add(new Person(3, 12, "name"));
        System.out.println(collection.contains(new Person(1, 12, "name")));
        //collection.remove(new Person(1, 12, "name"));
        Collection collection1 = new ArrayList();
        collection1.add(new Person(1, 12, "name"));
        collection1.add(new Person(2, 12, "name"));
        collection.retainAll(collection1);      //2个集合取交集、addAll，2个集合取并集，removeAll一个集合去掉另一个集合内容。
        collection.stream().forEach(System.out::println);









        ArrayList<Object> objects = new ArrayList<>();
        objects.add(1);
        objects.add(2);
        objects.add(3);
        objects.add(4);
        objects.add(4);
        System.out.println(objects.indexOf(4));
        System.out.println(objects.lastIndexOf(4));
        // list在remove索引与数据的区别
        objects.remove(1);
        objects.remove(new Integer(3));
        Iterator iterator = objects.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
        objects.set(1, 222);
        //左闭右开
        System.out.println(objects.subList(1, 2));

    }

}
