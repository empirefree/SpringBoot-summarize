package com.empirefree.mybatisplus;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.empirefree.mybatisplus.entity.User;
import com.empirefree.mybatisplus.service.IUserService;
import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.jasypt.encryption.StringEncryptor;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.SynchronousQueue;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
class MybatisplusApplicationTests {
    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    @Autowired
    private StringEncryptor encryptor;

    @Test
    public void synchronousQueueTest(){
        // 同步队列--生产者消费者
        SynchronousQueue<String> synchronousQueue = new SynchronousQueue<>();
        new Thread(()->{
            try {
                System.out.println(Thread.currentThread().getName()+"put 01");
                synchronousQueue.put("1");
                System.out.println(Thread.currentThread().getName()+"put 02");
                synchronousQueue.put("2");
                System.out.println(Thread.currentThread().getName()+"put 03");
                synchronousQueue.put("3");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(()->{
            try {
                System.out.println(Thread.currentThread().getName()+"take"+synchronousQueue.take());
                System.out.println(Thread.currentThread().getName()+"take"+synchronousQueue.take());
                System.out.println(Thread.currentThread().getName()+"take"+synchronousQueue.take());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    /**
     * Author: HuYuQiao
     * Description: 参考博客：https://blog.csdn.net/jeikerxiao/article/details/96480136
     */
    @Test
    public void encryptPassword(){
        String url = encryptor.encrypt("jdbc:mysql://localhost:3306/user?autoReconnect=true&serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=utf-8");
        String name = encryptor.encrypt("root");
        String password = encryptor.encrypt("root");
        System.out.println("database url: " + url);
        System.out.println("database name: " + name);
        System.out.println("database password: " + password);
        Assert.assertTrue(url.length() > 0);
        Assert.assertTrue(name.length() > 0);
        Assert.assertTrue(password.length() > 0);
    }

    @Test
    public void dequeTest(){
        Deque<Integer> deque = new ArrayDeque<>();
        deque.add(null);
        System.out.println(deque.isEmpty());
    }



    /**
     * Author: HuYuQiao
     * Description: 测试mysql是否解决了幻读
     * 1.没有了一级缓存，最后确实解决了幻读，说明mysql确实实现了可串行化。
     *
     */
    @Test
    @Transactional
    public void testCount(){
        System.out.println(userService.selectCount());
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(User::getId, 1);
        User user = new User();
        user.setName("胡宇乔~~~~~");
        userService.update(user, queryWrapper);
        System.out.println(userService.selectCount());
    }


    @Test
    public void treeMapTest(){
        SortedMap<String, Object> sortedMap = new TreeMap<>();
        sortedMap.put("1", 2);
        sortedMap.put("4", "3");
        sortedMap.put("4b", "d");
        sortedMap.put("3", "c");
        sortedMap.put("2b", "d");
        sortedMap.put("3b", "c");

        //value值降序排列
        Set<Map.Entry<String, Object>> entrySet = sortedMap.entrySet();
        List<Map.Entry<String, Object>> list = new ArrayList<>(entrySet);
        Collections.sort(list, new Comparator<Map.Entry<String, Object>>() {
            @Override
            public int compare(Map.Entry<String, Object> o1, Map.Entry<String, Object> o2) {
                return o1.getValue().toString().compareTo(o2.getValue().toString());
            }
        });
        for (Map.Entry<String, Object> entry : sortedMap.entrySet()){
            System.out.println(entry.getKey() + " " +  entry.getValue());
        }

        System.out.println("=========================");
        //Map遍历的2种方式
        for (Map.Entry<String, Object> entry : sortedMap.entrySet()){
            System.out.println(entry.getKey() + " " +  entry.getValue());
        }
        for (String key : sortedMap.keySet()){
            System.out.println(key);
        }
        for (Object value : sortedMap.values()){
            System.out.println(value);
        }
    }

    @Test
    @Transactional
    public void selectForUpdateTest(){
        //userService.selectOneUserShareLock(1);
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(User::getId, 1);
        User user = new User();
        String uuid = UUID.randomUUID().toString().replace("-", "");
        user.setName(uuid);
        log.info("UserController.testUpdate--{}", uuid);
        userService.selectOneUserForUpdate(1);
        userService.update(user, wrapper);
        userService.selectOneUserForUpdate(1);

    }
    @Test
    @Transactional
    public void selectForUpdateTest2(){
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(User::getId, 1);
        User user = new User();
        String uuid = UUID.randomUUID().toString().replace("-", "");
        user.setName(uuid);
        log.info("UserController.testUpdate--{}", uuid);
        //userService.update(user, wrapper);
        userService.selectOneUserShareLock(1);
        System.out.println("QAQ");
    }



    /**
     * 一级缓存：在一次事务中，若一个mapper文件没有进行过insert,update,delete，则多次select只会
     * 查询一次，中间有上述3种操作，就会刷新sqlsession.
     * 从普通select -> select..for update也会刷新sql session  ->学习一下尚硅谷的mysql内容
     *
     */
    @Test
    @Transactional
    public void mybatisCacheTest(){
        userService.selectOneUser(1);
        userService.selectOneUserForUpdate(1);

        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(User::getId, 2);
        User user = new User();
        user.setName("huyuqiao");
        userService.update(user, wrapper);
        userService.selectOneUserForUpdate(1);
        userService.selectOneUserForUpdate(1);
        userService.selectOneUser(1);


    }

    @Test
    public void stringTest(){
        // String内部重写了equals方法，所以如果使用equals方法比较的是实际对象的值，不重写的话则比较的是对象地址
        // 1.字符常量在jvm底层采用的是常量折叠的优化方式
        // 2.str1 + str2 采用的是stringBuilder.append方式进行代码拼接的，在堆上，所以实际并不是在栈中。
        String a = "str";
        String a2 = "str";
        String b = "ing";
        String b2 = "ing";
        System.out.println("常量池-比较常量地址: " + (a == a2) );     // true
        String c = a + b;
        System.out.println("堆-比较对象地址：" + ("string" == c));    // false
        System.out.println("堆-比较对象值：" + ("string".equals(c))); //true

        // final 定义后就是在常量池中
        final String aaa = "aaa";
        final String aaa2 = "aaa";
        System.out.println(aaa == aaa2);

        //以下创建了3个temp对象，第一次在字符串常量与堆中都创建了，第二次没有，第三次在堆中创建了.
        String temp  = new String("temp");
        String tmp = "temp";
        String temp2  = new String("temp");
        System.out.println(temp == temp2);
        System.out.println(temp == tmp);
    }

    @Test
    public void redisTest(){
        redisTemplate.opsForValue().set("huyuqiao", "1");
    }

    @Test
    void contextLoads() {
        List<User> userList = new ArrayList<>();

    }
    @Test
    public void HelloTest(){
    }

    @Data
    class Product{
        private Integer num;
    }

    /**
     * Author: HuYuQiao
     * Description: 只适用于单机
     */
    @Test
    public void bloomFilterTest(){
        // 插入1500个正数，容忍错误率为百分之0.01
        BloomFilter<Integer> filter = BloomFilter.create(
                Funnels.integerFunnel(),
                1500,
                0.01
        );
        System.out.println(filter.mightContain(1));
        System.out.println(filter.mightContain(2));
        filter.put(1);
        filter.put(2);
        System.out.println(filter.mightContain(1));
        System.out.println(filter.mightContain(2));
    }


    @Test
    public void jsonStringTest(){
        JSON.toJSONString("[\n" +
                "    {\n" +
                "        \"name\": \"品牌\",\n" +
                "        \"text\": {\n" +
                "            \"value\": \"\"\n" +
                "        },\n" +
                "        \"type\": 0,\n" +
                "        \"value\": \"\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"门店编码\",\n" +
                "        \"text\": {\n" +
                "            \"value\": \"\"\n" +
                "        },\n" +
                "        \"type\": 0,\n" +
                "        \"value\": \"\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"是否督导\",\n" +
                "        \"text\": {\n" +
                "            \"value\": \"\"\n" +
                "        },\n" +
                "        \"type\": 0,\n" +
                "        \"value\": \"\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"身份证号\",\n" +
                "        \"text\": {\n" +
                "            \"value\": \"\"\n" +
                "        },\n" +
                "        \"type\": 0,\n" +
                "        \"value\": \"\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"职称\",\n" +
                "        \"text\": {\n" +
                "            \"value\": \"\"\n" +
                "        },\n" +
                "        \"type\": 0,\n" +
                "        \"value\": \"\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"工号\",\n" +
                "        \"text\": {\n" +
                "            \"value\": \"202109081109\"\n" +
                "        },\n" +
                "        \"type\": 0,\n" +
                "        \"value\": \"\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"门店号\",\n" +
                "        \"text\": {\n" +
                "            \"value\": \"\"\n" +
                "        },\n" +
                "        \"type\": 0,\n" +
                "        \"value\": \"\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"门店名称\",\n" +
                "        \"text\": {\n" +
                "            \"value\": \"\"\n" +
                "        },\n" +
                "        \"type\": 0,\n" +
                "        \"value\": \"\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"渠道\",\n" +
                "        \"text\": {\n" +
                "            \"value\": \"\"\n" +
                "        },\n" +
                "        \"type\": 0,\n" +
                "        \"value\": \"\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"客户\",\n" +
                "        \"text\": {\n" +
                "            \"value\": \"\"\n" +
                "        },\n" +
                "        \"type\": 0,\n" +
                "        \"value\": \"\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"区域\",\n" +
                "        \"text\": {\n" +
                "            \"value\": \"\"\n" +
                "        },\n" +
                "        \"type\": 0,\n" +
                "        \"value\": \"\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"城市\",\n" +
                "        \"text\": {\n" +
                "            \"value\": \"\"\n" +
                "        },\n" +
                "        \"type\": 0,\n" +
                "        \"value\": \"\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"门店地址\",\n" +
                "        \"text\": {\n" +
                "            \"value\": \"\"\n" +
                "        },\n" +
                "        \"type\": 0,\n" +
                "        \"value\": \"\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"门店类型\",\n" +
                "        \"text\": {\n" +
                "            \"value\": \"\"\n" +
                "        },\n" +
                "        \"type\": 0,\n" +
                "        \"value\": \"\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"主管名称\",\n" +
                "        \"text\": {\n" +
                "            \"value\": \"\"\n" +
                "        },\n" +
                "        \"type\": 0,\n" +
                "        \"value\": \"\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"主管工号\",\n" +
                "        \"text\": {\n" +
                "            \"value\": \"\"\n" +
                "        },\n" +
                "        \"type\": 0,\n" +
                "        \"value\": \"\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"区域经理\",\n" +
                "        \"text\": {\n" +
                "            \"value\": \"\"\n" +
                "        },\n" +
                "        \"type\": 0,\n" +
                "        \"value\": \"\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"激活状态\",\n" +
                "        \"text\": {\n" +
                "            \"value\": \"\"\n" +
                "        },\n" +
                "        \"type\": 0,\n" +
                "        \"value\": \"\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"禁用状态\",\n" +
                "        \"text\": {\n" +
                "            \"value\": \"\"\n" +
                "        },\n" +
                "        \"type\": 0,\n" +
                "        \"value\": \"\"\n" +
                "    }\n" +
                "]");
    }

    @Autowired
    private IUserService userService;
    @Test
    public void testSelect(){
        //每一次查询都是一次sqlSession
        userService.selectOneUser(1);
        userService.selectOneUser(1);
    }
    @Test
    public void testGroupingBy(){
        List<Product> productList = new ArrayList<>();

        for (int i = 1; i < 100; i++) {
            Product product = new Product();
            product.setNum((int) (Math.random() * 100));
            productList.add(product);
        }
        Map<Integer, String> map = new HashMap<>();

        List<Map.Entry<Integer, List<Product>>> listGroupingByMap =
                productList
                        .stream()
                        .collect(Collectors.groupingBy(Product::getNum))
                        .entrySet()
                        .stream()
                        .sorted((a, b) -> -Integer.compare(a.getKey(), b.getKey()))
                        .collect(Collectors.toList());
        System.out.println(JSON.toJSONString(listGroupingByMap));
    }
}
