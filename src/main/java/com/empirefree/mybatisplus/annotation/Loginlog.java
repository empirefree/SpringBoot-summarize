package com.empirefree.mybatisplus.annotation;

import lombok.extern.java.Log;

import java.lang.annotation.*;

/**
 * @program: mybatisplus
 * @description:
 * @author: huyuqiao
 * @create: 2022/01/24 15:02
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Loginlog {


}
