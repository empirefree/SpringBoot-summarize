package com.empirefree.mybatisplus.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.empirefree.mybatisplus.entity.User;
import com.empirefree.mybatisplus.mapper.UserMapper;
import com.empirefree.mybatisplus.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * DysonLinkEngine
 *
 * 1.Request OTP
 * @see UserServiceImpl#selectOneUser(java.lang.Integer)
 *
 * @author empirefree
 * @since 2020-08-30
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    public static Queue<Integer> queue = new ArrayBlockingQueue<>(10);

    @Autowired
    UserMapper userMapper;

    public void setQueue(){
        queue.add(3);
    }
    @Override
    public User selectOneUser(Integer id) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(User::getId, id);
        return userMapper.selectOne(queryWrapper);
    }

    @Override
    public User selectOneUserForUpdate(Integer id) {
        return userMapper.selectOneUserForUpdate(id);
    }
    @Override
    public User selectOneUserShareLock(Integer id) {
        return userMapper.selectOneUserShareLock(id);
    }

    @Override
    public User selectOneUserWhere(Integer id) {
        return userMapper.selectOneUserWhere(id);
    }



    @Override
    public Integer selectCount() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        return userMapper.selectCount(queryWrapper);
    }

    @Override
    public Integer insertUser(User user) {
        return userMapper.insert(user);
    }
}
