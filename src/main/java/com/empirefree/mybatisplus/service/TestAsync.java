package com.empirefree.mybatisplus.service;

import com.empirefree.mybatisplus.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * @program: mybatisplus
 * @description:
 * @author: huyuqiao
 * @create: 2021/09/04 12:57
 */
@Service
@Slf4j
public class TestAsync {

    @Autowired
    Tranction tranction;


    @Async
    public Future<String> asyncFunction(){
        try {
            TimeUnit.SECONDS.sleep(5);
            log.info("异步处理完毕..." + Thread.currentThread().getName());
            tranction.insertOrRollback();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return AsyncResult.forValue("success");
    }
}
