package com.empirefree.mybatisplus.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.empirefree.mybatisplus.annotation.Loginlog;
import com.empirefree.mybatisplus.entity.FlatDO;
import com.empirefree.mybatisplus.entity.Time;
import com.empirefree.mybatisplus.entity.TimeRequest;
import com.empirefree.mybatisplus.entity.User;
import com.empirefree.mybatisplus.service.IUserService;
import com.empirefree.mybatisplus.service.TestAsync;
import com.empirefree.mybatisplus.service.impl.UserServiceImpl;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author empirefree
 * @since 2020-08-30
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    IUserService userService;
    @Autowired
    TestAsync testAsync;

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping("/ping")
    public void healthPing(){
        JSONObject jsonObject = restTemplate.getForObject("http://localhost:8080/test/user/flatUser", JSONObject.class);
        System.out.println("ping :" + jsonObject);
    }

    @RequestMapping("/flatUser")
    public FlatDO flatUser(){
        User user = userService.selectOneUserWhere(1);
        FlatDO flatDO = new FlatDO();
        flatDO.setUser(user);

        Queue queue = UserServiceImpl.queue;
        queue.add(Math.random());
        UserServiceImpl u = new UserServiceImpl();
        u.setQueue();
        System.out.println("queue = " + queue);
        return flatDO;
    }

    /**
     * Author: HuYuQiao
     * Description:
     *
     * 本地事务失效解决方案：https://www.bilibili.com/video/BV1np4y1C7Yf?p=285
     * 由于使用了cglib事务代理，故而不影响原始事务统计数据。
     *
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    @RequestMapping("/transactionRollback")
    public void transactionRollbackTest(){
        User user = new User();
        user.setName("原始事务");
        userService.insertUser(user);
        log.info("UserController.transactionRollbackTest--入库");
        UserController userController = (UserController) AopContext.currentProxy();
        /**
         * 开启了cglib事务代理：故而原始事务落库，而a()函数开启新事务自行处理
         */
/*        try {
            userController.a();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        /**
         * 1、test类不会出现幻读
         * 2、开启cglib类不会出现幻读
         *
         */
        System.out.println(userService.selectCount() + "==总数");
        /**
         *  由于未开始事务代理，a()函数与原始函数采用同一事务，原始事务try-catch了，不会回滚故而都落库了
         *  a()函数后续报错了，故而不会落库
         */
        try {
            a();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void a(){
        User user = new User();
        user.setName("a函数一");
        userService.insertUser(user);
        log.info("UserController.a--入库一");
        user.setName("a函数二");
        int a = 10 /0;
        log.info("UserController.a--入库二");
        userService.insertUser(user);

    }



/*    private ExecutorService threadPool = new ThreadPoolExecutor(
            3,
            10,
            3,
            TimeUnit.SECONDS,
            new LinkedBlockingDeque<>(20),
            Executors.defaultThreadFactory(),
            new ThreadPoolExecutor.DiscardOldestPolicy()
    );*/

    /**
     * @autowired修饰方法的时候会自动执行一次
     */
    @Autowired
    public void testAutowired(){
        System.out.println("asdf");
    }

    @PostMapping(value = "/testTimeTransfer")
    public Time testTimeTransfer(@RequestBody TimeRequest loginRequest){
        Time time = new Time();
        time.setLocalDateTime(loginRequest.getLocalDateTime());
        return time;
    }

    @RequestMapping(value = "/testAspect", method = RequestMethod.GET)
    public User testAspect(@RequestParam("name")String name){
        User user = new User();
        user.setName(name);
        return user;
    }
    private Integer a = new Integer(1999);

    @RequestMapping(value = "/exception",method = RequestMethod.POST)
    public Object findAll() throws Exception {
        throw new Exception("huyuqiao");
    }
    @RequestMapping(value = "/error",method = RequestMethod.POST)
    public Object error() throws Exception {
        System.out.println("UserController.error--全局异常处理");
        return "1";
    }

    /**
     * Author: HuYuQiao
     * Description:
     * 1 多个事务更新，以最后的更新为准，
     * 2 所以在对单条数据操作的时候，应该用for update 行级锁(同一事务中查后，其他事务更新、查询、删除会阻塞)
     * 3.
     *
     */
    @Transactional
    @RequestMapping(value = "/testUpdate", method = RequestMethod.GET)
    public void testUpdate() throws InterruptedException {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(User::getId, 1);
        User user = new User();
        String uuid = UUID.randomUUID().toString().replace("-", "");
        user.setName(uuid);
        log.info("UserController.testUpdate--{}", uuid);
        userService.update(user, wrapper);
        Thread.sleep(3000);
        log.info("UserController.testUpdate--等待3秒完成");

    }

    /**
     * Author: HuYuQiao
     * Description:
     *  1.可以看到，无论S,X锁（slect..for update/share in mode,insert,update,delete等当前读），都要读到库里最新的数据
     *  2.对于当前读这些，Mysql是通过Next-key lock(普通读是MVCC保证)来保证，即阻塞，保证下一次当前读能读到库里最新数据
     *
     */
    @Transactional
    @RequestMapping(value = "/testSelectForUpdate", method = RequestMethod.GET)
    public void testSelectForUpdate() throws InterruptedException {
        //log.info("UserController.testUpdate--原始uid:{}", userService.selectOneUserForUpdate(1).getName());
        log.info("UserController.testUpdate--原始uid:{}", userService.selectOneUserShareLock(1).getName());
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(User::getId, 1);
        User user = new User();
        String uuid = UUID.randomUUID().toString().replace("-", "");
        user.setName(uuid);
        log.info("UserController.testUpdate--{}", uuid);
        userService.update(user, wrapper);
        Thread.sleep(9000);
        log.info("UserController.testUpdate--等待9秒完成");

    }


    /**
     * Author: HuYuQiao
     * Description: 2种AOP
     *  1、直接采用AOP切面切入
     *
     *  2、自定义注解，采用注解的地方注入
     *
     */
    @Loginlog
    @RequestMapping(value = "/findAll",method = RequestMethod.POST)
    public Object findAll(@RequestParam(value = "huyuqiao", required = false)Integer a,  HttpServletRequest request, HttpServletResponse httpServletResponse) throws UnsupportedEncodingException {
        //获取前台发送过来的数据
        //ContentCachingRequestWrapper contentCachingRequestWrapper = new ContentCachingRequestWrapper(request);
        Integer pageNo = Integer.valueOf(request.getParameter("pageNo"));
        Integer pageSize = Integer.valueOf(request.getParameter("pageSize"));
        IPage<User> page = new Page<>(pageNo, pageSize);
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        val user = new User();
//        user.setId(1);
        wrapper.setEntity(user);

        HttpSession session = request.getSession();
//        session.setAttribute("huyuqiao", "huyuqiao");
        System.out.println(session.getAttribute("huyuqiao") + "session值");

        new ArrayList<>(3);

        Cookie cookie = new Cookie("HappyNewYear", URLEncoder.encode(request.getSession().getId(), "utf-8"));
        cookie.setPath("/");
        cookie.setMaxAge(48 * 60 * 60);
        httpServletResponse.addCookie(cookie);

        return userService.page(page,wrapper);
    }
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public void insert() throws ExecutionException, InterruptedException {
        log.info("进入代码...");
//        threadPool.submit(() ->  {
            log.info("进入线程池" + Thread.currentThread().getName());
            Future<String> stringFuture = testAsync.asyncFunction();
//            try {
//                System.out.println("异步处理返回: " + stringFuture.get());
//            } catch (InterruptedException | ExecutionException e) {
//                e.printStackTrace();
//            }
//        });
        System.out.println("进入后续代码...");
/*        log.info("进入代码...");
        //异步回调--有返回值
        CompletableFuture<Integer> future2 = CompletableFuture.supplyAsync(() ->{
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 3;
        });
        System.out.println("begin");
        future2.whenComplete((result, error) ->{
            System.out.println("返回结果:" + result);
            System.out.println("错误结果" + error);
        }).exceptionally(throwable -> {
            System.out.println(throwable.getMessage());
            return 502;
        });
        System.out.println("end");*/

    }



    @RequestMapping(value = "/testMybatisPlus", method = RequestMethod.GET)
    public List<User> testMybatisPlus(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
        // 1、lambda测试
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda() .eq(User::getName, "胡宇乔")
                .or()
                .eq(User::getId, 1);
        List<User> userList = userService.list(queryWrapper);

        Cookie cookie = new Cookie("HappyNewYear", URLEncoder.encode(request.getSession().getId(), "utf-8"));
        cookie.setPath("/");
        cookie.setMaxAge(48 * 60 * 60);     //默认是关闭浏览器失效，但是如果设置了过期时间，则浏览器会缓存
        response.addCookie(cookie);
        return userList;
    }
}
