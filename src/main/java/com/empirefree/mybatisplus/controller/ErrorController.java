package com.empirefree.mybatisplus.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.AbstractHandlerMethodMapping;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;

import java.util.*;

/**
 * @program: mybatisplus
 * @description:
 * 学习参考：
 * 方法二：https://blog.csdn.net/Sicily_winner/article/details/107714730
 * 本Controller用于跳转到404界面，有以下2种实现方式
 * 1. 全局异常捕获异常，然后内部跳转到404界面（简单常见）
 * 2. SpringMVC拦截器拦截，然后判断请求路径是否存在，依次来判断能否跳转到404界面（好蠢）
 * @author: huyuqiao
 * @create: 2022/02/08 16:15
 */
@Slf4j
@Controller
@RequestMapping("/error")
public class ErrorController {
    @Autowired
    ApplicationContext applicationContext;

/*    @GetMapping("*")
    public String notFound() {
        log.info("ErrorController.notFound--进入notFound界面");
        // 跳转到404页面所在路径
        return "404";
    }*/

    /**
     * Author: HuYuQiao
     * Description: 获取本系统所有请求路径
     */
    @ResponseBody
    @RequestMapping(value = "/getPath")
    public List getPath(){
        List<Object> list = new ArrayList<>();
        AbstractHandlerMethodMapping<RequestMappingInfo> objHandlerMethodMapping =
                (AbstractHandlerMethodMapping<RequestMappingInfo>) applicationContext.getBean("requestMappingHandlerMapping");
        Map<RequestMappingInfo, HandlerMethod> mapRet = objHandlerMethodMapping.getHandlerMethods();
        for (RequestMappingInfo requestMappingInfo :mapRet.keySet()){
            Set set = requestMappingInfo.getPatternsCondition().getPatterns();
            Iterator iterator = set.iterator();
            while (iterator.hasNext()){
                list.add(iterator.next().toString());
            }
        }

        return list;
    }

    /*404 错误页面*/
    @GetMapping("/404")
    public String error404() {
        log.info("ErrorController.error404--进入404");
        return "404";       //注：使用该注解需要pom与yaml导入对应thymeleaf依赖
    }
}
