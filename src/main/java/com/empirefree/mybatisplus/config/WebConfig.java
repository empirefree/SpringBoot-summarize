package com.empirefree.mybatisplus.config;

import com.empirefree.mybatisplus.interceptor.CustomerHandlerInterceptor;
import com.empirefree.mybatisplus.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @program: mybatisplus
 * @description:
 * @author: huyuqiao
 * @create: 2022/01/24 17:29
 */
/**
 * Spring管理Bean：https://blog.csdn.net/qq_38534144/article/details/82414201
 * 分为装配和注册,一般分为3种方式实现：xml:(@Component注解)、javaconfig(@Configuration与@Bean)、自动装配(@Autowired)
 *
 */

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Autowired
    IUserService userService;
    @Autowired
    ApplicationContext applicationContext;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new CustomerHandlerInterceptor(userService, applicationContext)).addPathPatterns("/**");
    }
}
