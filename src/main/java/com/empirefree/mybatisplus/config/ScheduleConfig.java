package com.empirefree.mybatisplus.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.concurrent.Executors;

/**
 * @program: mvn
 * @description:
 *
 * https://mp.weixin.qq.com/s/jNNSgNJbFaiJEKQBtaY1pQ
 * @author: huyuqiao
 * @create: 2022/09/16 17:47
 */
@Configuration
public class ScheduleConfig implements SchedulingConfigurer {
    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        //设定一个长度10的定时任务线程池
        taskRegistrar.setScheduler(Executors.newScheduledThreadPool(10));
    }
}
