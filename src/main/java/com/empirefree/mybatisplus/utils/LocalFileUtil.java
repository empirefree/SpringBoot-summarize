/**
 * Stjk.com Inc.
 * Copyright (c) 2016-2021 All Rights Reserved.
 *//*

package com.empirefree.mybatisplus.utils;


import com.amazonaws.util.IOUtils;
import com.empirefree.mybatisplus.entity.FileResultDTO;
import com.empirefree.mybatisplus.helper.JvmOSHelper;
import com.empirefree.mybatisplus.helper.StringHelper;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.*;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

*/
/**
 * @description: 本地文件处理
 * <p></p>
 * @author: cheng.zhou
 * @create: at 2021-07-16 17:29
 * @version: 1.0.0
 * @history: modify history
 * <author>              <time>              <version>              <desc>
 *//*

@Slf4j
public class LocalFileUtil {

    @Value("${upload.s3.switchP}")
    private String switchP;

    public static final String S3_PIC_DIR = "/smartSales/picType/";
    public static final String S3_VIDEO_DIR = "/smartSales/videoType/";

    */
/**
     *  单个文件上传本地
     * @param file
     * @param fileType
     * @return
     * @throws IOException
     *//*

    public static FileResultDTO localUploadWithSize(MultipartFile file, String fileType) throws IOException {
        log.info("本地文件上传开始");
        InputStream input = file.getInputStream();
        byte[] data = null;
        String tempPath = JvmOSHelper.projectDir() + S3_PIC_DIR;
        if ("video".equals(fileType)){
            tempPath = JvmOSHelper.projectDir() + S3_VIDEO_DIR;
        }
        File filemk = new File(tempPath);
        if (!filemk.exists()){
            filemk.mkdirs();
        }
        String originalFilename = file.getOriginalFilename();
        String fileName = UUID.randomUUID().toString().replaceAll("-", "") + originalFilename.substring(originalFilename.lastIndexOf("."));
        FileOutputStream fos = new FileOutputStream(new File(tempPath+fileName));
        try {
            //获取文件流
            data = new byte[input.available()];
            int len = 0;
            while ((len = input.read(data)) != -1) {
                fos.write(data, 0, len);
            }
            log.info("本地文件上传成功");
        } catch (IOException e) {
            log.info("文件上传失败，异常信息",e);
        } finally {
            if (input != null) {
                try {
                    input.close();
                    fos.close();
                } catch (IOException e) {
                    log.info("input关闭异常",e);
                }
            }
        }
        FileResultDTO fileResultDTO = new FileResultDTO();
        fileResultDTO.setPath(S3_PIC_DIR+fileName);
        fileResultDTO.setSize(StringHelper.formatFileSize(file.getSize()));

        return fileResultDTO;
    }

    */
/**
     *  多个文件上传本地
     * @param file
     * @param fileType
     * @return
     * @throws IOException
     *//*

    public static List<FileResultDTO> localUploadMore(List<MultipartFile> file, String fileType) throws IOException {
        List<FileResultDTO> list = new ArrayList<>();
        if (!CollectionUtils.isEmpty(file)){
            for (MultipartFile multipartFile : file) {
                FileResultDTO fileResultDTO = localUploadWithSize(multipartFile, fileType);
                list.add(fileResultDTO);
            }

        }

        return list;
    }

    */
/**
     * 根据url获取MultipartFile
     * @param url
     * @return
     *//*

    public static MultipartFile getMultipartFileByUrl(String url){
        return getMultipartFile(getFileByUrl(url));
    }

    */
/**
     * 根据url获取file文件
     * @param url
     * @return
     *//*

    public static File getFileByUrl(String url){
        String fileName = url.substring(url.lastIndexOf("."), url.length());
        File file = null;
        URL urlfile;
        InputStream inStream = null;
        OutputStream os = null;
        try {
            file = File.createTempFile("video", fileName);
            //下载
            urlfile = new URL(url);
            inStream = urlfile.openStream();
            os = new FileOutputStream(file);

            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = inStream.read(buffer, 0, 8192)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != os) {
                    os.close();
                }
                if (null != inStream) {
                    inStream.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return file;
    }

    */
/**
     * 根据file获取MultipartFile
     * @param file
     * @return
     *//*

    public static MultipartFile getMultipartFile(File file) {
        FileItem item = new DiskFileItemFactory().createItem("file"
                , MediaType.MULTIPART_FORM_DATA_VALUE
                , true
                , file.getName());
        try (InputStream input = new FileInputStream(file);
             OutputStream os = item.getOutputStream()) {
            // 流转移
            IOUtils.copy(input, os);
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid file: " + e, e);
        }
        return new CommonsMultipartFile(item);
    }


}*/
