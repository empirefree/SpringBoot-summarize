package com.empirefree.mybatisplus.huyuqiao;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.date.Month;
import cn.hutool.core.swing.clipboard.ClipboardUtil;
import cn.hutool.core.util.CharsetUtil;

import java.awt.datatransfer.Clipboard;
import java.time.Instant;
import java.util.Date;

/**
 * @program: mvn
 * @description:
 * @author: huyuqiao
 * @create: 2022/08/24 17:03
 */

public class hutools {
    //1. 类型转换工具类
    public static void typeChange(){
        String a = "我是一个小小的可爱的字符串";
        System.out.println(Convert.toHex(a, CharsetUtil.CHARSET_UTF_8));

        // 发票金额啊
        System.out.println(Convert.digitToChinese(71431.32));
        // 数字转中文
        System.out.println(Convert.numberToChinese(123.234, false));
        System.out.println(Convert.numberToChinese(123.234, true));
        //数字转英文
        System.out.println(Convert.numberToWord(1001.23));
    }

    public static void dateUse(){
        String dateStr = "2022-08-25";
        Date date = DateUtil.parseDate(dateStr);
        System.out.println("转换的date: " + date);
        String temp= DateUtil.format(date, "yyyy/MM/dd");
        System.out.println("自定义转换的string格式: " + temp);

        // ISO时间要多8小时，不能直接转换
        String isoDateStr = "2018-10-01T10:00:00Z";
        date = DateUtil.parseDate(isoDateStr);
        System.out.println("isoDate: " + date);
        DateTime dateTime = new DateTime(Instant.parse(isoDateStr)) ;
        System.out.println("dateTime: " + dateTime);

        System.out.println("-------------------------");
        //获取年、月、的美剧
        Date date1 = DateUtil.date();
        System.out.println(DateUtil.year(date1));
        Month monthEnum = DateUtil.monthEnum(date);
        System.out.println(monthEnum.getValue());
        for(Month m : Month.values()){
            System.out.println(m.getValue());
        }

        System.out.println("offsetDay: " +  DateUtil.offsetDay(DateUtil.parse("2022-08-24 00:00:00", "yyyy-MM-dd HH:mm:ss"), 3));

    }

    public static void ClipbordUse(){
        Clipboard clipboard = ClipboardUtil.getClipboard();
        System.out.println("获取系统剪贴板: " + clipboard);
        System.out.println(ClipboardUtil.getStr() + " " + ClipboardUtil.getImage());

    }
    public static void main(String[] args) {
        //typeChange();
        //dateUse();
        ClipbordUse();

    }
}
