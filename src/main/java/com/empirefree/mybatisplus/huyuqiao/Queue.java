package com.empirefree.mybatisplus.huyuqiao;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

/**
 * @program: mvn
 * @description:
 * @author: huyuqiao
 * @create: 2022/02/25 17:02
 */
@Slf4j
public class Queue {
    /**
     * 入队：
     *  Offer -> 队列没满，立即返回true，满了立即返回false,不阻塞(add 添加失败会报错)
     *  Put  -> 队列满了，一直阻塞，队列不满则不阻塞
     *  Offer特殊情况：设置放入时间，到了时间没能放入也会返回false
     * 出队：
     *  Poll -> 没有元素返回null，有则出对(remove返回错误)
     *  Take -> 没有元素则一直阻塞
     *  Poll特殊情况：设置取出时间，到了时间没能取出返回null
     *
     *  以下内容未知准确性
     *  参考博客：https://www.jianshu.com/p/7b2f1fa616c6
     *  ArrayBlockingQueue：一个对象数组+一把锁，入队出队共用该锁，需指定容量
     *  LinkedBlockingQueue：一个单向链表+两把锁，最大容量为整数最大值.
     *  内部源码采用automatic实现队列容量的安全性
     *
     */
    public static void main(String[] args) throws InterruptedException {
        ArrayBlockingQueue arrayBlockingQueue = new ArrayBlockingQueue<String>(1);
        arrayBlockingQueue.offer("a", 2, TimeUnit.SECONDS);
        //arrayBlockingQueue.put("a");
        System.out.println(arrayBlockingQueue.take());
        arrayBlockingQueue.remove();
        System.out.println(arrayBlockingQueue.poll());
        System.out.println(arrayBlockingQueue.take());
        arrayBlockingQueue.offer("a", 2, TimeUnit.SECONDS);
        log.info("Queue.main--队列大小:{}", arrayBlockingQueue.size());
    }
}
