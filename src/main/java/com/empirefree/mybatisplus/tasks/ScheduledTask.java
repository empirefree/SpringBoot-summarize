package com.empirefree.mybatisplus.tasks;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.xml.crypto.Data;
import java.time.LocalDateTime;

/**
 * @program: mvn
 * @description:
 * @author: huyuqiao
 * @create: 2022/09/16 17:41
 */
@Component
public class ScheduledTask {
    @Scheduled(cron = "${jobs.cron}")
    public void getTask2() {
        System.out.println("任务2,从配置文件加载任务信息，当前时间：" + LocalDateTime.now());
    }
}
