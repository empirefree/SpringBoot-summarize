package com.empirefree.mybatisplus.enums;

/**
 * @program: mybatisplus
 * @description:
 * @author: huyuqiao
 * @create: 2022/02/15 17:26
 */

public enum LimitTypeEnum implements BaseEnum<LimitTypeEnum, Short>{
    /**
     * 自定义key
     */
    CUSTOMER,

    /**
     * 请求者IP
     */
    IP;

    ;

    @Override
    public Short getValue() {
        return null;
    }

    @Override
    public String getLabel() {
        return null;
    }
}
