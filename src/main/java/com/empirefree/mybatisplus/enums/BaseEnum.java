package com.empirefree.mybatisplus.enums;

/**
 * @Description
 * @Author andy
 */
public interface BaseEnum<E extends Enum<?>, T> {

    /**
     * 值
     *
     * @return
     */
    T getValue();

    /**
     * 描述
     *
     * @return
     */
    String getLabel();
}