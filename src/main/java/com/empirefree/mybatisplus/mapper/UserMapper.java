package com.empirefree.mybatisplus.mapper;

import com.empirefree.mybatisplus.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author empirefree
 * @since 2020-08-30
 */
public interface UserMapper extends BaseMapper<User> {
    User selectOneUserForUpdate( @Param("id") Integer id);
    User selectOneUserShareLock( @Param("id") Integer id);
    User selectOneUserWhere( @Param("id") Integer id);

}
