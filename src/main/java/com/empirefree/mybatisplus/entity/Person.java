package com.empirefree.mybatisplus.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @program: mvn
 * @description:
 * @author: huyuqiao
 * @create: 2022/03/16 10:44
 */
@Data   //@Setter、@Getter、@RequiredArgsConstructor、@ToString、@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class Person {
    private Integer id;

    private Integer age;
    private String name;

    public void function(){
        testFunction();
    }

    public void testFunction(){
        System.out.println("person");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
