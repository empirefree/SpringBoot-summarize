package com.empirefree.mybatisplus.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: mvn
 * @description:
 * @author: huyuqiao
 * @create: 2022/03/19 16:04
 */
@Data
@NoArgsConstructor
public class SonPerson extends Person{

    public SonPerson(int i, int i1, String s) {
    }

    @Override
    public void testFunction() {
        System.out.println("asdf");
    }

    /**
     * Author: HuYuQiao
     * Description: 可以看到，子类对父类函数的重写，调用父类的函数就是采用子类
     */
    public static void main(String[] args) {
        SonPerson sonPerson = new SonPerson();
        sonPerson.function();
    }
}
