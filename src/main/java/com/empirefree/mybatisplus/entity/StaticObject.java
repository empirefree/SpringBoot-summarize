package com.empirefree.mybatisplus.entity;

/**
 * @program: mybatisplus
 * @description: 用于测试static关键字
 * 作用域
 * 1.
 *
 * @author: huyuqiao
 * @create: 2022/02/12 14:45
 */

public class StaticObject {
    //非静态变量
    private String str1 ="property";
    //静态变量
    public static String str2 ="staticProperty";

    public StaticObject() {
    }

    //静态方法
    public static void print2() {
        //Non-static field 'str1' cannot be referenced from a static context
        //str1;
        System.out.println(str2);
        //Cannot resolve method 'print1' in 'StaticObject'
        //print1();
    }
    static {
        System.out.println("静态代码块");
    }

    private static String staticString = "ab";
    private static Integer staticInt = 1;
    public static void main(String[] args) {
        System.out.println("============");
        String a = "a";
        String b = "b";
        String commonString = a + b;
        final String finalString = "ab";
        String newString = new String("ab");
        System.out.println("ab" == commonString);   //在堆中定义，故而不相等
        System.out.println("ab" == finalString);    //==,equals都等于true说明final String 定义的数据在字符串常量池（堆）
        System.out.println("ab" == staticString);   //==,equals都等于true说明static定义的数据在堆中字符串常量池中
        System.out.println(finalString == staticString);    //final 定义为常量亦是在字符串常量池中。
        System.out.println("ab" == newString);

    }

}
