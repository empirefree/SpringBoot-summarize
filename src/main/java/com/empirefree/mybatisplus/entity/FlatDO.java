package com.empirefree.mybatisplus.entity;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @program: mybatisplus
 * @description:
 * @author: huyuqiao
 * @create: 2022/02/21 11:32
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FlatDO {
    @JsonUnwrapped
    private User user;
}
