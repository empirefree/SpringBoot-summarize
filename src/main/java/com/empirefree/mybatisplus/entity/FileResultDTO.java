/**
 * Stjk.com Inc.
 * Copyright (c) 2016-2021 All Rights Reserved.
 */
package com.empirefree.mybatisplus.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @description:文件上传返回体
 * <p></p>
 * @author: cheng.zhou
 * @create: at 2021-06-04 10:44
 * @version: 1.0.0
 * @history: modify history
 * <author>              <time>              <version>              <desc>
 */
@Data
@ToString
public class FileResultDTO implements Serializable {
    private static final long serialVersionUID = 1784732614376999295L;

    private String path;

    private String size;
}