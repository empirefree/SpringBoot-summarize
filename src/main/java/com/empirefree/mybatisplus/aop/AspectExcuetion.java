//package com.empirefree.mybatisplus.aop;
//
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.springframework.core.annotation.Order;
//import org.springframework.stereotype.Component;
//
///**
// * @program: mybatisplus
// * @description:
// * @author: huyuqiao
// * @create: 2022/02/15 18:29
// */
//@Aspect
//@Component
//@Order(0)               //设置优先级，值越低优先级越高
//public class AspectExcuetion {
//    /**
//     * 第一个表示匹配任意的方法返回值， …(两个点)表示零个或多个，
//     * 第一个…表示service包及其子包
//     * 第二个表示所有类
//     * 第三个*表示所有方法，第二个…表示方法的任意参数个数
//     */
//    @Around(value = "execution(* com.empirefree.mybatisplus.controller..*.*(..))")
//    public void processAuthority (ProceedingJoinPoint point)throws Throwable{
//        System.out.println("EXECUTION welcome");
//        System.out.println("EXECUTION 调用方法:" + point.getSignature().getName());
//        System.out.println("EXECUTION 目标对象：" + point.getTarget());
//        System.out.println("EXECUTION 首个参数：" + point.getArgs()[0]);
//        point.proceed();
//        System.out.println("EXECUTION success");
//
//    }
//}
