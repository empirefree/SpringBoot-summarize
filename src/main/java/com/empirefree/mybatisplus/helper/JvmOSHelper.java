/*
package com.empirefree.mybatisplus.helper;

import javafx.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.lang.reflect.Field;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;

*/
/** JVM信息工具类 **//*

public final class JvmOSHelper {
    private static final Logger LOG = LoggerFactory.getLogger(JvmOSHelper.class);
    private JvmOSHelper() {
    }
    private static final double SIZE_M = 1024 * 1024D;
    @SuppressWarnings("unused")
    private static volatile String OS_NAME, OS_ARCH, FILE_SEPARATOR, PROJECT_DIR;
    */
/** 是否Window操作系统 **//*

    public static boolean isWindows() {
        return ofValue(OS_NAME, ()-> System.getProperty("os.name")).toUpperCase().startsWith("WIN");
    }
    */
/** 获取操作系统版本 **//*

    public static boolean isV64() {
        return ofValue(OS_ARCH, ()-> System.getProperty("os.arch")).endsWith("64");
    }
    */
/** JVM HeapMemory，key=MAX、value=USED **//*

    public static Pair<Double, Double> heapMemory() {
        MemoryMXBean memoryMXBean = ManagementFactory.getMemoryMXBean();
        MemoryUsage heapMemory = memoryMXBean.getHeapMemoryUsage();
        return new Pair<>(heapMemory.getMax()/SIZE_M, heapMemory.getUsed()/SIZE_M);
    }
    */
/** JVM DirectMemory，key=MAX、value=USED **//*

    public static Pair<Double, Double> directMemory() {
        try {
            Class<?> c = Class.forName("java.nio.Bits");
            Field reservedM = c.getDeclaredField("reservedMemory");
            boolean accessible = reservedM.isAccessible();
            reservedM.setAccessible(true);
            synchronized (reservedM) {
                long used = ((AtomicLong) reservedM.get(null)).get();
                reservedM.setAccessible(accessible);
                return new Pair<>(sun.misc.VM.maxDirectMemory()/SIZE_M, used/SIZE_M);
            }
        } catch (Exception e) {
            LOG.error("Get used direct memory error ", e);
            return new Pair<>(sun.misc.VM.maxDirectMemory()/SIZE_M, 0D);
        }
    }
    */
/** 不同系统的文件分隔符 **//*

    public static String fileSeparator() {
        return ofValue(FILE_SEPARATOR, () -> System.getProperty("file.separator"));
    }
    */
/** 获取工程所在目录 **//*

    public static String projectDir() {
        return ofValue(PROJECT_DIR, ()-> System.getProperty("user.dir"));
    }
    */
/** 获取指定包下指定子类型的CLASS列表 **//*

    public static <T> Set<Class<? extends T>> classesSubOf(String basePackage, final Class<T> typed) {
        Reflections reflections = new Reflections(basePackage);
        return reflections.getSubTypesOf(typed);
    }
    */
/** 获取指定包下指定注解的CLASS列表 **//*

    public static Set<Class<?>> classesAnnotatedWith(String basePackage, final Class<? extends Annotation> annotated) {
        Reflections reflections = new Reflections(basePackage);
        return reflections.getTypesAnnotatedWith(annotated);
    }

    private static String ofValue(String rs, Supplier<String> supplier) {
        if(StringHelper.isBlank(rs)) {
            rs = supplier.get();
        }
        return rs;
    }
}
*/
