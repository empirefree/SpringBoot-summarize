package com.example.mvn.entity;

import lombok.Data;

/**
 * @program: demo
 * @description:
 * @author: huyuqiao
 * @create: 2022/02/23 15:33
 */
@Data
public class Hello {
    private String name;

}
